package com.appetiser.baseplate.resources.ext

import android.app.Activity
import android.util.DisplayMetrics
import android.widget.Toast

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Activity.getDeviceHeight(): Int {
    return DisplayMetrics().apply {
        this@getDeviceHeight.windowManager.defaultDisplay.getMetrics(this)
    }.heightPixels
}

fun Activity.getDeviceWidth(): Int {
    return DisplayMetrics().apply {
        this@getDeviceWidth.windowManager.defaultDisplay.getMetrics(this)
    }.heightPixels
}