package com.appetiser.baseplate.resources.ext

import java.util.regex.Pattern
import android.os.Build
import android.text.Html
import android.text.Spanned


fun String.isEmailValid(): Boolean {
    return Pattern.compile(
        "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern
            .CASE_INSENSITIVE
    ).matcher(this).find()
}


@Suppress("DEPRECATION")
@SuppressWarnings("deprecation")
fun String.toHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun String.isUpperCase(): Boolean {
    return this.single().isUpperCase()
}

fun String.isLowerCase(): Boolean {
    return this.single().isLowerCase()
}
